
//Define Class Notebook functionality
var ClassNotebook = (function(){

    var notebook = {};

    // Define constants for ONENOTE API interaction
    const ONENOTE_API_NOTEBOOKS_URL = "https://edog.onenote.com/api/v1.0/me/notes/notebooks/";
            
    const ONENOTE_API_PAGES_URL = "https://edog.onenote.com/api/v1.0/me/notes/pages/";
            
    const ONENOTE_API_COPYPAGE_URL = "https://edog.onenote.com/api/v1.0/me/notes/pages/{pageId}/CopyToSection/";
            
    const ONENOTE_API_COPYPAGE_OPERATION_STATUS_URL = "https://edog.onenote.com/api/beta/me/notes/operations/";
            
    const ONENOTE_API_SECTION_PAGES_URL = "https://edog.onenote.com/api/v1.0/me/notes/sections/{sectionId}/pages";
    
    var ONENOTE_API_REQUEST_HEADERS = {
        'MS-Int-AppId' : 'Class Notebook Add-in',
        'Authorization' : 'Bearer {AccessToken}'
    };
    
    function initializeWithAccessToken(accessToken)
    {
        ONENOTE_API_REQUEST_HEADERS['Authorization'] = ONENOTE_API_REQUEST_HEADERS['Authorization'].replace("{AccessToken}", accessToken);
    }
    
    function distributeActivePage(destinationSectionName)
    {
        var startTime = new Date();
        app.showNotification('Started Distribute Page at : ' + startTime);
        progressBarToShow(0);
        // Get the active page and sync this page - Prototype assumes sync is done
        // Get the page ID using the API (Pages API ?)
        // Invoke a batch to copy the page
        return OneNote.run(function (context) {
                var page = context.application.getActivePage();
                var section = context.application.getActiveSection();
                context.load(page, "ClientUrl");
                context.load(section, "ClientUrl");
                return context.sync()
                .then(function() {
                    progressBarToShow(10);
                    var apiRequestUrl = ONENOTE_API_PAGES_URL;
                    jQuery.ajax({
                        type: "GET",
                        url: apiRequestUrl,
                         data:{
                           $filter: "links/oneNoteClientUrl/href eq '" + page.clientUrl + "'"
                        },
                        contentType: "application/json",
                        headers: ONENOTE_API_REQUEST_HEADERS,
                        success: function (pages, status, jqXHR) {
                            app.showNotification('Retrieved information about the page being distributed');
                            var page = pages.value[0];
                            var sections = getSectionsByName(destinationSectionName);
                            var copyPagesPromiseList = [];
                            progressBarToShow(20);
                            sections.forEach(function(section, index) {
                                copyPagesPromiseList.push(copyPageToSection(page.id, section.id));
                            });
                            return Promise.all(copyPagesPromiseList)
                            .then(function() {
                                var copyPagesOperationIdList = [];
                                copyPagesPromiseList.forEach(function(copyPagesPromise, index) {
                                    copyPagesPromise.then(function(copyPagesResponse) {
                                        copyPagesOperationIdList.push(copyPagesResponse.id);
                                    });
                                });
                                progressBarToShow(30);
                                return pollCopyPagesStatus(copyPagesOperationIdList, startTime);
                            });
                        },
                        error: function (jqXHR, status) {
                            app.showNotification("Distribute page failed to get page details with result code : " + jqXHR);
                        }
                    });
                });
        });
    }

    function reviewStudentWork()
    {
        var startTime = new Date();
        app.showNotification("Started Review at :" + startTime);
        queryClassNotebookHierarchy().then(function() {
            var endTime = new Date();
            app.showNotification("Review Completed at " + endTime + " and took " + (endTime - startTime)/1000 + " seconds");
        });
    }


    // Queries the ONENOTE REST API for this Class Notebook's hierarchy (Notebooks, section groups, sections and pages)
    function queryClassNotebookHierarchy()
    {
        return OneNote.run(function (context) {
            // Get notebook
            var notebookProxy = context.application.getActiveNotebook();
            notebookProxy.load("clientUrl");
            return context.sync().then(function() {
                var notebookFilterValue = notebookProxy.clientUrl.replace(/\/$/, "");
                var pagesQuery = getPagesInNotebook(notebookFilterValue);
                
                // Read Section Groups, student Sections in that notebook
                var hierarchyQuery = jQuery.ajax({
                    type: "GET",
                    url: ONENOTE_API_NOTEBOOKS_URL,
                    data:{
                        $filter: "links/oneNoteClientUrl/href eq '" + notebookFilterValue + "'",
                        $expand: "sectionGroups($expand=sections)"
                    },
                    contentType: "application/json",
                    headers: ONENOTE_API_REQUEST_HEADERS
                });
                // Read pages in each section
                return hierarchyQuery.then(function(notebooks) {
                    app.showNotification("Hierarchy query finished : " + new Date());
                    notebook = notebooks.value[0];
                    return pagesQuery.then(function(pages) {
                        app.showNotification("Pages query finished : " + new Date());
                        notebook.sectionGroups.forEach(function(sectionGroup, index) {
                            sectionGroup.sections.forEach(function(section, index) {
                                section.pages = pages.value.filter(function(page) {
                                    return page.parentSection.id == section.id;
                                });
                            });
                        });
                    })
                    .fail(function(xhr, textStatus, error) {
                        app.showNotification("Failed to query pages : " + xhr.statusText);
                    });
                })
                .fail(function(xhr, textStatus, error) {
                    app.showNotification("Failed to get hierarchy : " + xhr.statusText);
                });
            });    
        })
        .then(function() {
            return notebook;
        });
    }
    
    function getPagesInNotebook(notebookClientUrl)
    {
        var apiRequestUrl = ONENOTE_API_PAGES_URL;
        return jQuery.ajax({
                    type: "GET",
                    url: apiRequestUrl,
                    data:{
                       $top: 100,
                       $filter: "startswith(links/oneNoteClientUrl/href, '" + notebookClientUrl + "')"
                    },
                    contentType: "application/json",
                    headers: ONENOTE_API_REQUEST_HEADERS
        });
    }
    
    function getSectionsByName(sectionName)
    {
        var sections = [];
        notebook.sectionGroups.forEach(function(sectionGroup, index) {
            var sectionsInSectionGroup = sectionGroup.sections.filter(function(section) {
                return section.name === sectionName;
            });
            sections = sections.concat(sectionsInSectionGroup);
        });
        return sections;
    }
    
    function copyPageToSection(pageId, sectionId)
    {
        var apiRequestUrl = ONENOTE_API_COPYPAGE_URL.replace("{pageId}", pageId);
        var apiRequestHeaders = ONENOTE_API_REQUEST_HEADERS;
        return jQuery.ajax({
                    type: "POST",
                    url: apiRequestUrl,
                    processData: false,
                    data: JSON.stringify({id: sectionId}),
                    contentType: "application/json",
                    headers: apiRequestHeaders
                });
    }
    
    function pollCopyPagesStatus(copyOperationIdList, startTime)
    {
        var apiRequestUrl = ONENOTE_API_COPYPAGE_OPERATION_STATUS_URL;
        
        var completedCopyOperations = 0;
        var runningCopyOperations = 0;
        var notStartedOperations = 0;
        var copyOperationPromiseList = [];
        copyOperationIdList.forEach(function(copyOperationId, index)
        {
            copyOperationPromiseList.push(jQuery.ajax({
                    type: "GET",
                    url: ONENOTE_API_COPYPAGE_OPERATION_STATUS_URL + copyOperationId + "/",
                    processData: false,
                    contentType: "application/json",
                    headers: ONENOTE_API_REQUEST_HEADERS
            }));
        });
        return Promise.all(copyOperationPromiseList)
            .then(function() {
                var operationStatusList = [];
                copyOperationPromiseList.forEach(function(copyOperationResponse, index) {
                    operationStatusList.push(JSON.parse(copyOperationResponse.responseText).status);
                });
                notStartedOperations = operationStatusList.filter(function(copyOperationStatus) {
                    return copyOperationStatus == "not started";
                }).length;
                runningCopyOperations = operationStatusList.filter(function(copyOperationStatus) {
                    return copyOperationStatus == "running";
                }).length;
                completedCopyOperations = operationStatusList.filter(function(copyOperationStatus) {
                    return copyOperationStatus == "completed";
                }).length;
            })
            .then(function() {
                console.log("Not Started :" + notStartedOperations + " Running : " + runningCopyOperations + " Completed : " + completedCopyOperations);
                app.showNotification("Completed Copy Operations : " + completedCopyOperations + " of Total : " + copyOperationPromiseList.length);
                var completedPercentage = completedCopyOperations / copyOperationIdList.length * 35;
                var runningPercentage = (runningCopyOperations + completedCopyOperations) / copyOperationIdList.length * 35;
                if(completedCopyOperations < copyOperationPromiseList.length)
                {
                    progressBarToShow(completedPercentage + runningPercentage + 30);
                    setTimeout(function() {
                        pollCopyPagesStatus(copyOperationIdList, startTime);
                    }, 1000);
                }
                else
                {
                    var endTime = new Date();
                    app.showNotification("Completed all copy operations at : " + endTime);
                    app.showNotification("Distribute took " + (endTime - startTime) /1000 + " seconds");
                    showStatus("success");
                    progressBarToShow(0);
                }
            });
    }

    function progressBarToShow(percentage){
        $("#progressBar").css('width', percentage+'%').attr('aria-valuenow', percentage);
    }
    
    return { "initializeWithAccessToken" : initializeWithAccessToken, "reviewStudentWork" : reviewStudentWork, "distributeActivePage" : distributeActivePage, "queryClassNotebookHierarchy" : queryClassNotebookHierarchy };
    
})();