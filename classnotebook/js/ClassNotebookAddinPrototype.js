// Define Office add-in initialization
Office.initialize = function(reason) {
    jQuery(document).ready(function() {
        app.initialize();
        ClassNotebook.initializeWithAccessToken('eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Ikk2b0J3NFZ6QkhPcWxlR3JWMkFKZEE1RW1YYyIsImtpZCI6Ikk2b0J3NFZ6QkhPcWxlR3JWMkFKZEE1RW1YYyJ9.eyJhdWQiOiJodHRwczovL29mZmljZWFwcHMubGl2ZS5jb20iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC83MmY5ODhiZi04NmYxLTQxYWYtOTFhYi0yZDdjZDAxMWRiNDcvIiwiaWF0IjoxNDc1ODgzMzQxLCJuYmYiOjE0NzU4ODMzNDEsImV4cCI6MTQ3NTg4NzI0MSwiYWNyIjoiMSIsImFtciI6WyJwd2QiXSwiYXBwaWQiOiJkMzU5MGVkNi01MmIzLTQxMDItYWVmZi1hYWQyMjkyYWIwMWMiLCJhcHBpZGFjciI6IjAiLCJlX2V4cCI6MTA4MDAsImZhbWlseV9uYW1lIjoiQ2hlbiIsImdpdmVuX25hbWUiOiJKdW5yZW4iLCJpbl9jb3JwIjoidHJ1ZSIsImlwYWRkciI6IjEzMS4xMDcuMTQ3Ljc4IiwibmFtZSI6Ikp1bnJlbiBDaGVuIiwib2lkIjoiZTcyMGM0OTAtNjQ5ZC00ZmE0LWFhYTMtNjI0NDRkYWFlMjE4Iiwib25wcmVtX3NpZCI6IlMtMS01LTIxLTIxMjc1MjExODQtMTYwNDAxMjkyMC0xODg3OTI3NTI3LTIxODQ3MDE5IiwicHVpZCI6IjEwMDMzRkZGOThCQTE4NTYiLCJzY3AiOiJ1c2VyX2ltcGVyc29uYXRpb24iLCJzdWIiOiJSd0twWlJvT1BQM1lBMmRlWmJTMXVib29DVFFVR3JRYk5jVzhyazloRmZZIiwidGlkIjoiNzJmOTg4YmYtODZmMS00MWFmLTkxYWItMmQ3Y2QwMTFkYjQ3IiwidW5pcXVlX25hbWUiOiJqdW5jaEBtaWNyb3NvZnQuY29tIiwidXBuIjoianVuY2hAbWljcm9zb2Z0LmNvbSIsInZlciI6IjEuMCJ9.OI124COmR_9cq9p-lUWbTCh5D1wDMiUEGAEtOHQKyhfWgq7UJck2N9EmvG-I0ANJYDgcvzwdg3To5_B_9dzVx31z6ztQTxwsi0IZymSoBt3HEKvmB6pPFUonHhtwMRCstSQzPxDb2Gm4afm-BddNC5x2qNVbhHiv6JKIj9Pu9Rbi807a4aG9NIXhAsTHuqrdalgUhcaRZ3gl0hegGvV26XMNsWFsFQrbW4g5OPx2vDQPiudbXtE2yJunBDTSlAMuYn3mwovIED6MZUhge8kfp6ibH_IbDihpNN-Z-H0Rx9l--QiwGDMzQfXwgKIo11Zol21upBM5DN9dyFEzyrVxvA');
        ClassNotebook.queryClassNotebookHierarchy().then(function(notebook) {
            //notebook->sectionGroups->sections->pages
            setPageTitle();
            console.log(constructSectionMap_WAC(notebook));
            var sectionSet = traverseNotebook(notebook);
            setUpListForSection(sectionSet);
            finishLoadingContent();
        });
    });
};