var studentSectionGroupObjects;
var sectionMap;
var ctx;
var notebook;

Office.initialize = function () {
    setPageTitle();
    ctx = new OneNote.RequestContext();
    getStudents(ctx)
    .then(function(studentSectionGroups){
        studentSectionGroupObjects = studentSectionGroups;
        return fullyLoadNoteBook(ctx,2);
    })
    .then(function(){
        return constructSectionMap(ctx);
    })
    .then(function(map){
        sectionMap = map;
        setUpListForSection(sectionMap);
        finishLoadingContent();
    }).catch(function(error){
        errorHandler(error, "statusDiv");
    });

    // setPageTitle();
    // ctx = new OneNote.RequestContext();
    // loadNotebook().then(function(notebookObject){
    //     notebook = notebookObject;
    //     sectionMap = constructSectionMap_UWP(notebookObject);
    //     setUpListForSection(sectionMap);
    //     finishLoadingContent();
    // }).catch(function(error){
    //      errorHandler(error, "statusDiv");
    // });
};

function distributePageForStudents(){
    var studentNames = [];
    var sectionPath = $("#section_list .selected").find("span").html();
    $.each(studentSectionGroupObjects, function(index, studentSectionGroupObject){
        studentNames.push(studentSectionGroupObject.name);
    });
    return distributePage(studentNames, [sectionPath], ctx);
}

function setPageTitle() {
    var currentPage;
    OneNote.run(function(ctx) {
        currentPage = ctx.application.getActivePage();
        currentPage.load("title");
		return ctx.sync()
            .then(function() {
                $("#page1_title1").html("Distribute " + currentPage.title + " to student section");
            });
	})
	.catch(function(error) {
        errorHandler(error, "statusDiv");
	});
}

function setUpListForSection(sectionMap){
    for(var sectionPath of sectionMap.keys()){
        var spanElement = $("<span>" + sectionPath + "</span>").addClass("sectionSpanStyle");
        var sectionImage = $("<image src=" + getRandomSectionImage() + "></image>").addClass("sectionImageStyle").prop("aria-hideen", true);
        $('<li></li>').append(sectionImage).append(spanElement).appendTo("#section_list").click(function(){
            $('li').removeClass('selected');
            $(this).addClass('selected');
            showStatus("");
            $("#distributeAction").prop("disabled", false);
        });
    }
}

function distributePage(students, sectionPaths, ctx){
    //show loading animation and disable buttons.
    $("#distributeAction").prop("disabled", true);
    showStatus("loading");
    var targetSections = [];
    var section_to_create = new Map();
    var pageToCopy;
    $.each(sectionPaths, function(index, sectionPath){
        var studentMap = sectionMap.get(sectionPath);
        $.each(students, function(index, student){
            if(studentMap.has(student)){
                targetSections.push(studentMap.get(student));
            }
            else{
                section_to_create.set(student, sectionPath);
            }
        })
    })

    return createSections(section_to_create, ctx)
    .then(function(newlyCreatedSections){
        targetSections = targetSections.concat(newlyCreatedSections);
        pageToCopy = ctx.application.getActivePageOrNull();
        pageToCopy.load();
        return ctx.sync();  
    })
    .then(function(){
       if(pageToCopy.isNull){
           throw new classNotebookError("this is invalid page");
       } 
       $.each(targetSections, function(index, targetSection){
           var newPage = pageToCopy.copyToSection(targetSection);
           newPage.load();
       });

       return ctx.sync(); 
    }).then(function(){
        $.each(targetSections, function(index, targetSection){
            ctx.trackedObjects.remove(targetSection);
        });
        return ctx.sync();
    }).then(function(){
        showStatus("success");      
    }).catch(function(error){
        //show text and enable button
        errorHandler(error, "statusDiv");
        showStatus("fail");
        $("#section_div-button-div :button").prop("disabled", false);
    });
}

function createSections(section_to_create, ctx){
    // return newly created sections

    var promises = [];
    for(var studentName of section_to_create.keys()){
        var sectionPath = section_to_create.get(studentName);
        var sectionGroupNames = [];
        var subSectionGroupOrSection = sectionPath.split("/");
        for(i = 0; i < subSectionGroupOrSection.length - 1; i++){
            sectionGroupNames.push(subSectionGroupOrSection[i]);
        }
        // last one is section, rest are sectionGroups
        var sectionName = subSectionGroupOrSection[subSectionGroupOrSection.length - 1];
        
        //get student sectionGroup
        var studentSectionGroupObject = studentSectionGroupObjects.find(function(sectionGroup){
            return sectionGroup.name === studentName;
        });

        var sectionPromise = createSectionGroupAndSectionRecursively(sectionGroupNames, 0, studentSectionGroupObject, sectionName, ctx);
        promises.push(sectionPromise);
    }

    return Promise.all(promises);
}

function createSectionGroupAndSectionRecursively(sectionGroupNames, index, sectionGroupObject, sectionName, ctx){
    if(sectionGroupNames.length == index){
        // we have run out all sectionGroup, check if target section exists or not, if not, create one.
        var targetSection = hasCertainSectionOrSectionGroup(sectionGroupObject, sectionName, "section");
        if(targetSection == null){
            targetSection = sectionGroupObject.addSection(sectionName);
        }

        targetSection.load();
        ctx.trackedObjects.add(targetSection);
        return ctx.sync(targetSection);
    }
    else{
        // we have not run out all sectionGroups, try to go to next section group, if does not exist, create one.
        var targetSectionGroupName = sectionGroupNames[index];
        var targetSectionGroup = hasCertainSectionOrSectionGroup(sectionGroupObject, targetSectionGroupName, "sectionGroup");
        if(!targetSectionGroup){
            targetSectionGroup = sectionGroupObject.addSectionGroup(targetSectionGroupName);
        }
        targetSectionGroup.load();
        targetSectionGroup.sections.load();
        targetSectionGroup.sectionGroups.load();

        return ctx.sync()
        .then(function(){
            return createSectionGroupAndSectionRecursively(sectionGroupNames, index + 1, targetSectionGroup, sectionName, ctx);
        });
    }
}

function distributePageForStudentGroup(){
    var studentNames = new Set();
    var validStudentNames = [];
    var studentGroupNames = [];
    var sectionPaths = [];
    $("#studentGroup_div input:checked").each(function() {
        studentGroupNames.push($(this).attr('name'));
    });

    $("#section_div input:checked").each(function() {
        sectionPaths.push($(this).attr('name'));
    });

    $.each(studentGroupNames, function(index, studentGroupName){
        var studentGroup = getStudentGroup(studentGroupName);
        for(let studentName of studentGroup.keys()){
            studentNames.add(studentName);
        }

        // filter invalid studentName with notebook
        for(let studentName of studentNames.keys()){
            var contains = false;
            for(let studentSectionGroupObject of studentSectionGroupObjects){
                if(studentSectionGroupObject.name === studentName){
                    contains = true;
                    break;
                }
            }

            if(contains){
                validStudentNames.push(studentName);
            }
        }
    });  

    // return distributePage(studentNames, sectionPaths, ctx);
    console.log(validStudentNames);
    console.log(studentNames);
}


function finishLoadingContent(){
    $("#contentLoadingPage").hide();
    $("#content").show();
}

function getRandomSectionImage(){
    var num = Math.floor((Math.random() * 16) + 1);
    return "../Images/Section" + num + ".png";
}

function distributePageThroughRestApi(){

    $("#distributeAction").prop("disabled", true);
    showStatus("loading");

    var sectionPath = $("#section_list .selected").find("span").html();
    ClassNotebook.distributeActivePage(sectionPath);
}