Office.initialize = function () {
    //setPageTitle();
};

function setPageTitle() {
    var notebook;
    return OneNote.run(function(ctx) {
        notebook = ctx.application.getActiveNotebook();
        notebook.load("name");
		return ctx.sync()
            .then(function() {
                $("#title_div").text("Distribute new section to " + notebook.name);
            });
	})
	.catch(function(error) {
		console.log("Error: " + JSON.stringify(error));
	});
}

function enableOrDisableButton() {
    $("#create").prop("disabled", $("#page1-input_div-sectionName").val().length === 0);
}

function distributeNewSection(){
    //if user specify sectionGroup, then create empty section in sectionGroup(if student doesn't have certain sectionGroup, then create one)
    //else create empty section under student
    //if student already has target section, then do nothing....

    //disable button and show loading div 
    $("#create").prop("disabled", true);
    showStatus("loading");

    var targetSectionName = $("#page1-input_div-sectionName").val();
    var ctx = new OneNote.RequestContext();
    getStudents(ctx).then(function(students){
        $.each(students, function(index, student){
            if(!checkSectionGroupContainsCertainSection(student, targetSectionName)){
                var newSection = student.addSection(targetSectionName);
                newSection.load();
            }
        })
        return ctx.sync();
    }).then(function(){
        showStatus("success");
        //Office.context.ui.messageParent(0);
    }).catch(function(error){
        showStatus("fail");
        errorHandler(error, "statusDiv");
    });
}

function checkSectionGroupContainsCertainSection(sectionGroup, targetSectionName){
    var contains = false;
    $.each(sectionGroup.sections.items, function(index, section){
        if(section.name === targetSectionName){
            contains = true;
        }
    });
    return contains;
}
