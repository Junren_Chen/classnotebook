Office.initialize = function () {
    setPageTitle();
};

function setPageTitle() {
    var notebook;
    OneNote.run(function(ctx) {
        notebook = ctx.application.getActiveNotebook();
        notebook.load("name");
		return ctx.sync()
            .then(function() {
                    $("h1").html("Distribute new section group to " + notebook.name);
            });
	})
	.catch(function(error) {
		console.log("Error: " + JSON.stringify(error));
	});
}

function enableOrDisableButton() {
    $("#create").prop("disabled", $("#page1-input_div-sectionGroupName").val().length === 0);
}

function distributeSectionGroup() {
    var sectionGroups;
    var newSectionGroupName = $("#page1-input_div-sectionGroupName").val();
    var ctx = new OneNote.RequestContext();
    getStudents(ctx)
    .then(function(students){
        $.each(students, function(index, student){
            var targetSectionGroup = hasCertainSectionOrSectionGroup(student, newSectionGroupName, "sectionGroup");
            if(targetSectionGroup == null){
                targetSectionGroup = student.addSectionGroup(newSectionGroupName);
            }
            targetSectionGroup.load();
        });
        return ctx.sync();
    }).catch(function(error){
        console.log("Error: " + error);
    });
}