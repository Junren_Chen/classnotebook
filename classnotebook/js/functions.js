var dialogHandler;

Office.initialize = function() {};

function openTabForCreateClassNotebook(args){
	window.open("https://www.onenote.com/classnotebook/creator");
	args.completed();
}

function openTabForManageStudent(args){
	window.open("https://www.onenote.com/classnotebook/manage-students");
	args.completed();
}

function openTabForManageTeacher(args){
	window.open("https://www.onenote.com/classnotebook/manage-teachers");
	args.completed();
}

function openTabForGetNotebookLink(args){
	window.open("https://aka.ms/classnotebookaddinonenoteforteachers");
	args.completed();
}

function OpenTabForTeacherTraining(args){
	window.open("http://www.onenoteforteachers.com");
	args.completed();
}

function OpenTabForAddinGuide(args){
	window.open("https://aka.ms/classnotebookaddinonenoteeducation");
	args.completed();
}

function OpenTabForEducationBlog(args){
	window.open("https://aka.ms/classnotebookaddinonenoteeducationc");
	args.completed();
}

function OpenTabForEducatorCommunity(args){
	window.open("https://aka.ms/classnotebookaddinmicrosofteducation");
	args.completed();
}

function openTabToSendFeedback(args){
	window.open("mailto:classnotebook@onenote.uservoice.com");
	args.completed();
}

function OpenTabForViewKnowledgeBase(args){
	window.open("https://aka.ms/classnotebookaddinknowledgebase");
	args.completed();
}


function openTabForSuggestingFeature(args){
	window.open("https://aka.ms/classnotebookaddinsuggestfeature");
	args.completed();
}

function openDialogForDistributeSection(args){
	Office.context.ui.displayDialogAsync(
			'https://localhost:9999/classnotebook/distributeSection.html',
			{
				width: 35,
				height: 30,
				displayInIframe: true
			},
			function (dialogProxy) {
				dialogHandler = dialogProxy.value;
				dialogHandler.addEventHandler(
					Microsoft.Office.WebExtension.EventType.DialogMessageReceived,
					dialogMessageHandler
				);
			}
	);
	args.completed();
}

function dialogMessageHandler(event) {
	switch (event.message) {
		case 0:
			dialogHandler.close();
		break;
	}
}