Office.initialize = function ()
{
    ctx = new OneNote.RequestContext();
    fullyLoadNoteBook(ctx, 1)
        .then(function ()
        {
           return constructSectionMap(ctx);
        })
        .then(function (map)
        {
            sectionMap = map;
            setUpSection(map);
            initializeComponent();
        }).then(finishLoading)
        .catch(function (error)
        {
            errorHandler(error, "statusDiv");
        });
};

function setUpSection(sectionMap)
{
    var it = sectionMap.keys();
    var sectionPath = it.next();
    while (!sectionPath.done) 
    {
        $("<option>" + sectionPath.value + "</option>").appendTo("#sectionPathDropdown").click(onclickChangeForSection);
        sectionPath = it.next();
    }
}

function setUpStudent(pageMap, ctx, comparator)
{
    //ensure student list is clean
    $("#student_div-list").empty();

    var studentInfos = [];
    var it = pageMap.keys();
    var clientUrl = it.next();
    while (!clientUrl.done)
    {
        var studentName = pageMap.get(clientUrl.value);
        var names = studentName.split(" ");
        var studenInfo = {
            firstName: names[0],
            lastName: names.length == 1 ? "" : names[names.length - 1],
            url: clientUrl.value
        };
        studentInfos.push(studenInfo);
        clientUrl = it.next();
    }

    studentInfos.sort(comparator);

    $.each(studentInfos, function (index, studentInfo)
    {
        var spanElement = $("<div>" + studentInfo.firstName + " " + studentInfo.lastName + "</div>").prop("style", "padding-top:12px; padding-left:5px;").val(studentInfo.url);
        $("<li></li>").append(spanElement).appendTo("#student_div-list").addClass("studentStyle")
            .click(function ()
            {
                $('#student_div-list > li').removeClass('selectedStudent');
                $(this).addClass('selectedStudent');
                var currentUrl = $(this).find("div").val();
                ctx.application.navigateToPageWithClientUrl(currentUrl);
                return ctx.sync()
            });
    });
}

function onclickChangeForSection()
{
    var sectionPath = $("#sectionPathDropdown :selected").text();

    $("#pageDropdown").hide();
	$("#studentListDiv").hide();
    $("#pageDropdown .ms-Dropdown").remove();
    $("#pageLoadingDiv").show();
    getAllDistributedPageInTargetSectionPath(ctx, sectionMap.get(sectionPath))
        .then(function (titleMap)
    {
		setUpPages(titleMap);
        $("#pageDropdown").show();
        $("#pageLoadingDiv").hide();
     })
}

function setUpPages(titleMap)
{
	//add dropdown element
	var label = $("<label></label>").addClass("ms-Label").text("Distributed pages");
	var icon = $("<i></i>").addClass("ms-Dropdown-caretDown ms-Icon ms-Icon--ChevronDown");
	var select = $("<select></select>").addClass("ms-Dropdown-select").change(function (){onclickChangeForPage(titleMap)});
	var defaultOption = $("<option></option>").prop("disabled", true).prop("selected", true).text("Choose a Page...").appendTo(select);

	var it = titleMap.keys();
	var title = it.next();
	while (!title.done)
	{
		var pageMap = titleMap.get(title.value);
        $("<option>" + title.value + "</option>").appendTo(select);
		title = it.next();
	}
	$("<div></div>").append(label, icon, select).addClass("ms-Dropdown").appendTo("#pageDropdown").Dropdown();
}


function onclickChangeForPage(titleMap)
{
	var pageTitle = $("#pageDropdown select :selected").text();
    var sortRule = $(".ms-ContextualMenu-link.is-selected").parent().val();
	setUpStudent(titleMap.get(pageTitle), ctx, getComparator(sortRule));
	$("#studentListDiv").show();
}

function sortStudentName(sortRule)
{
    var studentInfos = $("#student_div-list").find("div").toArray();
    var pageMap = new Map();
    $.each(studentInfos, function (index, studentInfo)
    {
        var studentName = studentInfo.innerText;
        var studentURL = studentInfo.value;
        pageMap.set(studentURL, studentName);
    });
    setUpStudent(pageMap, ctx, getComparator(sortRule));
}

function getAllDistributedPageInTargetSectionPath(ctx, studentToSectionMap)
{
    // return map: [pagetitle -> [pageurl -> studentName]]
    var it = studentToSectionMap.values();
    var sectionObject = it.next();
    while (!sectionObject.done)
    {
        //load page with title and clientUrl
        sectionObject.value.pages.load("title, clientUrl");
        sectionObject = it.next();
    }
    return ctx.sync()
        .then(function ()
        {
            var titleMap = new Map();
            var it = studentToSectionMap.keys();
            var student = it.next();
            while (!student.done)
            {
                var sectionObject = studentToSectionMap.get(student.value);
                $.each(sectionObject.pages.items, function (index, page)
                {
                 if (!titleMap.has(page.title))
                    {
                       titleMap.set(page.title, new Map());
                    }
                    titleMap.get(page.title).set(page.clientUrl, student.value);
                });
                student = it.next();
            }
            return titleMap;
         })
}

function getComparator(sortRule)
{
  switch (sortRule){
    case 0:
    // sort by first name 
        return function (a, b)
        {
            if (a.firstName == b.firstName)
            {
                return a.lastName > b.lastName ? 1 : -1;
            }
            else
            {
                return a.firstName > b.firstName ? 1 : -1;
            }
        }

   case 1:
    //sort by last name
        return function (a, b)
        {
            if (a.lastName == b.lastName)
            {
                return a.firstName > b.firstName ? 1 : -1;
            }
            else
            {
                return a.lastName > b.lastName ? 1 : -1;
            }
        }
    }
}

function onclickForSortOption()
{
	var optionText = $(event.currentTarget).find("a").text();
	var optionIndex = $(event.currentTarget).val();
    sortStudentName(optionIndex);
	$("#selectedSortOptions").text(optionText);
	$("#sortOptions").hide();
}

function showSortOptions()
{
	$("#sortOptions").show();
}

function initializeComponent(){
    $("#sectionOptions").Dropdown();
	$("#sortOptions").ContextualMenu();
    $("#sectionOptions").find("span").prop("style", "background:url('../Images/Section5.png') no-repeat 8px; padding-left:40px;");
}