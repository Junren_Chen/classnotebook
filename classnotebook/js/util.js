function checkSectionGroupIsStudent(sectionGroup){
    return !sectionGroup.name.startsWith("_");
}

function getStudents(ctx) {
    //load student sectionGroup and return it
    //by now, you have to fully load all sectionGroup in notebook
    //if you only load some certain sectionGroups, those sectionGroups may not load properly.

    var students = [];
    var sectionGroups = ctx.application.getActiveNotebook().sectionGroups;
    sectionGroups.load();
    return ctx.sync()
        .then(function(){
            $.each(sectionGroups.items, function(index, sectionGroup){
                if(checkSectionGroupIsStudent(sectionGroup)){
                    students.push(sectionGroup)
                }
                sectionGroup.load();
                sectionGroup.sections.load();
                sectionGroup.sectionGroups.load();
            });
            return ctx.sync(students);
        });
}


function majorityItems(sectionGroups, sectionOrSectionGroup, count){
    var result = [];
    var itemMap = new Map();
    $.each(sectionGroups, function(index, sectionGroup){
        var items = sectionOrSectionGroup === "section" ? sectionGroup.sections.items : sectionGroup.sectionGroups.items;
        $.each(items, function(index, item){
            if(!itemMap.has(item.name)) {
                itemMap.set(item.name, []);
            }
           var value = itemMap.get(item.name).push(item);
        });
    });

    for(var value of itemMap.values()){
        if(value.length / count >= 0.5){
            result = result.concat(value);
        }
    }

    return result;
}


function hasCertainSectionOrSectionGroup(sectionGroup, name, type){
    //return sectionGroup if student has certain section group
    //else return null
    var result = null;
    var candidates = type == "section" ? sectionGroup.sections.items : sectionGroup.sectionGroups.items;
    $.each(candidates, function(index, candidate){
        if(candidate.name === name){
            result = candidate;
        }
    });

    return result;
}

function loadSectionGroupRecursively(sectionGroups, ctx){
    if(sectionGroups.length == 0){
        return;
    }
    $.each(sectionGroups, function(index, sectionGroup){
        sectionGroup.sections.load();
        sectionGroup.sectionGroups.load();
    })

    return ctx.sync()
    .then(function(){
        var nextLevelSectionGroups = [];
        $.each(sectionGroups, function(index, sectionGroup){
            if(sectionGroup.sectionGroups.count > 0){
                $.each(sectionGroup.sectionGroups.items, function(index, nextLevelSectionGroup){
                    nextLevelSectionGroup.load();
                    nextLevelSectionGroup.parentSectionGroupOrNull.load();
                    nextLevelSectionGroups.push(nextLevelSectionGroup);
                });
            }
            if(sectionGroup.sections.count > 0){
                $.each(sectionGroup.sections.items, function(index, section){
                    section.load();
                    section.parentSectionGroupOrNull.load();
                });
            }
        });

        return ctx.sync(nextLevelSectionGroups);
    })
    .then(function(nextLevelSectionGroups){
       return loadSectionGroupRecursively(nextLevelSectionGroups, ctx);
    });
}

function fullyLoadNoteBook(ctx, count){
    if(count == 0){
        return;
    }
    // load all sections and sectionGroups in notebook
    var sgs = ctx.application.getActiveNotebook().sectionGroups;
    sgs.load();
    return ctx.sync()
    .then(function(){
        return loadSectionGroupRecursively(sgs.items, ctx);
    }).then(function(){
        return fullyLoadNoteBook(ctx, count - 1);
    })
    .catch(function(error) {
        console.log("Error: " + error);
        if (error instanceof OfficeExtension.Error) {
            console.log("Debug info: " + JSON.stringify(error.debugInfo));
        }
    });
}

function constructSectionMap(ctx){
    //will return a map of sections, which over 50% students have and group by section path and studentnames.
    //if section is in: Student->SG1->SG2->SG3->section, then section path will be SG1.name->SG2.name->SG3.name->section.name
    //sectionMap: [sectionPath -> [studentName -> sectionObject]]

    var sections_to_navigate;
    return getStudents(ctx)
    .then(function(students){
        return getSections(students, students.length, ctx);
    })
    .then(function(sections){
        var promises = [];
        sections_to_navigate = sections;
        $.each(sections, function(index, section){
            var sectionPathPromise = getSectionPath(section, ctx);
            promises.push(sectionPathPromise);
        });

        return Promise.all(promises);
    })
    .then(function(values){
        var map = new Map();
        $.each(values, function(index, value){
            if(!map.has(value.path)){
                map.set(value.path, new Map());
            }
            map.get(value.path).set(value.student, sections_to_navigate[index]);
        });

        return map;
    }).catch(function(error){
        console.log("Error: " + error);
    });
}

function getSections(sectionGroups, studentCount, ctx){
    //will return sections which over 50% student have.
    if(sectionGroups.length === 0){
        return [];
    }
    var nextLevelSectionGroups = majorityItems(sectionGroups, "sectionGroup", studentCount);
    var currentSections = majorityItems(sectionGroups, "section", studentCount);

    //TODO: this can be optimized, only need to load certain nested sectionGroups
    $.each(sectionGroups, function(index, sectionGroup){
        if(sectionGroup.sectionGroups.count > 0){
            $.each(sectionGroup.sectionGroups.items, function(index, nextLevelSectionGroup){
                nextLevelSectionGroup.sections.load();
                nextLevelSectionGroup.sectionGroups.load();
            });
        }
    });

    return ctx.sync()
    .then(function(){
        return getSections(nextLevelSectionGroups, studentCount, ctx);
    })
    .then(function(sections){
        return currentSections.concat(sections);
    })
    .catch(function(error){
        console.log("Error: " + error);
    });
}


function getSectionPath(sectionOrSectionGroup, ctx){
    if(sectionOrSectionGroup.isNull){
        return {student:"", path:""};
    }
    var parentSectionGroup = sectionOrSectionGroup.parentSectionGroupOrNull;
    parentSectionGroup.load();
    return ctx.sync()
    .then(function(){
        return getSectionPath(parentSectionGroup, ctx);
    })
    .then(function(result){
        if(result.student.length === 0){
            result.student = sectionOrSectionGroup.name;
        }
        else{
            result.path = result.path + (result.path.length > 0 ? "/" : "") + sectionOrSectionGroup.name;
        }

        return result;
    }).catch(function(error){
        console.log("Error: " + error);
    });
}

function checkCurrentBookIsClassNotebook(ctx){
    //check current notebook has "_Collaboration Space" and "_Content Library" sectionGroup or not.
    var sectionGroups = ctx.application.getActiveNotebook().sectionGroups;
    sectionGroups.load();
    return ctx.sync()
    .then(function(){
        return hasCertainSectionOrSectionGroup(sectionGroups,"_Collaboration Space", "sectionGroup") 
        && hasCertainSectionOrSectionGroup(sectionGroups,"_Content Library", "sectionGroup");
    });
}

function finishLoading(){
    //TODO: show some other contents if this note book is not class notebook
    $("#loading_page").hide();
    $("#content").show();
}


function errorHandler(error, placeToShow){
    console.log("Error" + error);
    if(error instanceof OfficeExtension.Error || error instanceof classNotebookError){
        $("#" + placeToShow).text(error);
    }
}

function classNotebookError(message){
    this.name = "classNotebookError";
    this.message = message || "something wrong with class notebook"
}

function showStatus(statusToShow){
    //There are three divs, only show one of them
    if(statusToShow === "loading"){
        $("#loading_spinner").show();
        $("#SuccessfulStatus").hide();
        $("#failedStatus").hide();
    }
    else if(statusToShow === "success"){
        $("#loading_spinner").hide();
        $("#SuccessfulStatus").show();
        $("#failedStatus").hide();
    }
    else if(statusToShow === "fail"){
        $("#loading_spinner").hide();
        $("#SuccessfulStatus").hide();
        $("#failedStatus").show();
    }
    else{
        //hide all of them
        $("#loading_spinner").hide();
        $("#SuccessfulStatus").hide();
        $("#failedStatus").hide();
    }
}

function traverseNotebook(notebook){
    var sectionSet = new Set();
    $.each(notebook.sectionGroups, function(index, sectionGroup){
        if(checkSectionGroupIsStudent(sectionGroup)){
            $.each(sectionGroup.sections, function(index, section){
                sectionSet.add(section.name);
            })
        }
    })
    return sectionSet;
}


function constructSectionMap_WAC(notebook){
    //will return a map of sections, which over 50% students have and group by section path and studentnames.
    //if section is in: Student->SG1->SG2->SG3->section, then section path will be SG1.name->SG2.name->SG3.name->section.name
    //sectionMap: [sectionPath -> [studentName -> sectionObject]]

    var studentSG = [];
    $.each(notebook.sectionGroups, function(index, sectionGroup){
        if(checkSectionGroupIsStudent(sectionGroup)){
            studentSG.push(sectionGroup);
        }
    });

    var map = new Map();
    $.each(studentSG, function(index, student){
        constructSectionMapHelper_WAC(student, map, student.name, "");
    });

    return map;
}

function constructSectionMapHelper_WAC(sectionGroup, map, studentName, path){
    var containsSection = sectionGroup.sections && sectionGroup.sections.length > 0;
    var containsSectionGroup = sectionGroup.sectionGroups && sectionGroup.sectionGroups.length > 0;
    if(containsSection){
        $.each(sectionGroup.sections, function(index, section){
            var currentPath = path + (path.length == 0 ? "" : "/")+ section.name;
            if(!map.has(currentPath)){
                map.set(currentPath, new Map());
            }

            map.get(currentPath).set(studentName, section);
        });
    }

    if(containsSectionGroup){
        $.each(sectionGroup.sectionGroups, function(index, nextLevelSectionGroup){
            constructSectionMap_WAC(nextLevelSectionGroup, map, studentName, path + (path.length == 0 ? "" : "/" ) + nextLevelSectionGroup.name);
        });
    }
} 

function constructSectionMap_UWP(notebook){   
    var studentSG = [];
    $.each(notebook.sectionGroups.items, function(index, sectionGroup){
        if(checkSectionGroupIsStudent(sectionGroup)){
            studentSG.push(sectionGroup);
        }
    });

    var map = new Map();
    $.each(studentSG, function(index, student){
        constructSectionMapHelper_UWP(student, map, student.name, "");
    });

    return filterWithMajoritySection(map, studentSG.length);
}


function constructSectionMapHelper_UWP(sectionGroup, map, studentName, path){
    var containsSection = sectionGroup.sections.count > 0;
    var containsSectionGroup = sectionGroup.sectionGroups.count > 0;
    if(containsSection){
        $.each(sectionGroup.sections.items, function(index, section){
            var currentPath = path + (path.length == 0 ? "" : "/")+ section.name;
            if(!map.has(currentPath)){
                map.set(currentPath, new Map());
            }

            map.get(currentPath).set(studentName, section);
        });
    }

    if(containsSectionGroup){
        $.each(sectionGroup.sectionGroups.items, function(index, nextLevelSectionGroup){
            constructSectionMapHelper_UWP(nextLevelSectionGroup, map, studentName, path + (path.length == 0 ? "" : "/" ) + nextLevelSectionGroup.name);
        });
    }
} 

function loadNotebook(){
    return new Promise(function(resolve){
        OneNote.run(function (ctx) {
            console.log(new Date());                    
            var notebook = ctx.application.getActiveNotebook();
            ctx.loadRecursive(notebook,
            {
              NoteBook: 'sections, sectionGroups, name',
              SectionGroup: 'sections, sectionGroups, name',
              Section: 'pages, name'
            });
            return ctx.sync().then(function() {
                console.log(new Date());    
                return resolve(notebook); 
            });
        });
    });
}

function filterWithMajoritySection(map, studentCount){
    var result = new Map();
    for(var sectionPath of map.keys()){
        var studentMap = map.get(sectionPath);
        if(studentMap.size / studentCount >= 0.5){
            result.set(sectionPath, studentMap);
        }
    }
    return result;
}

$(document).ready(function(){
  var $spinners = $(".ms-Spinner"); 
  //var spinnerInstance = fabric.Spinner($spinner);
  $spinners.each(function(){
    fabric.Spinner(this);
  })
});

classNotebookError.prototype.toString = function (){
    return this.message;
}